#include "Adafruit_VL53L0X.h"
#include <TFT_eSPI.h> // Hardware-specific library
#include <SPI.h>
#include <BLEDevice.h>
#include <BLEServer.h>
#include <BLEUtils.h>
#include <BLE2902.h>
#include <ros.h>
#include <std_msgs/String.h>
#include <std_msgs/Float64.h>

// ----------- GLOBAL VARIABLE ----------
unsigned long previousTimeScreen = 0;
unsigned long previousTimeROS = 0;
unsigned long  previousTimeToF = 0 ;
unsigned long LetterDetectedDelay = 0 ;
unsigned int LetterCounter = 0 ;
bool lock_status = true;
unsigned long distance = 0 ;
// ----------- BLE ----------------------
BLEServer* pServer = NULL;
BLECharacteristic* pCharacteristic = NULL;
bool deviceConnected = false;
bool oldDeviceConnected = false;
String request_BLE = "";
#define SERVICE_UUID        "4fafc201-1fb5-459e-8fcc-c5c9c331914b"
#define CHARACTERISTIC_UUID "beb5483e-36e1-4688-b7f5-ea07361b26a8"
// ----------- CAPTEUR ToF ---------------
TFT_eSPI tft = TFT_eSPI();       // Invoke custom library
Adafruit_VL53L0X lox = Adafruit_VL53L0X();
//------------ ROS ----------------------
ros::NodeHandle nh;
std_msgs::Float64 motor_msg ;
ros::Publisher motor_cmd("/tilt_controller/command", &motor_msg);


//-------------------------------------------------------------
class MyServerCallbacks: public BLEServerCallbacks {
    void onConnect(BLEServer* pServer) {
      deviceConnected = true;
      Serial.println("Connection done");
    };

    void onDisconnect(BLEServer* pServer) {
      deviceConnected = false;
      Serial.println("Deconnected");
    }
};

class MyCallbacks: public BLECharacteristicCallbacks {
    void onWrite(BLECharacteristic *pCharacteristic) {
      std::string value = pCharacteristic->getValue();
      request_BLE = value.c_str();
      Serial.println(request_BLE);
      }
    void onRead(BLECharacteristic *pCharacteristic) {
      
      std::string myStringForUnit16( (char*)&LetterCounter, 2 );
      pCharacteristic->setValue( myStringForUnit16 );
      
    } 
};

void setup() {
  // put your setup code here, to run once:
    
  setup_Serial();
  setup_ToF();
  setup_Screen();
  setup_BLE();
  setup_ros();

}

void loop() {
  // put your main code here, to run repeatedly:
  
  if ( (millis() - previousTimeToF) > 50 and ( (millis() - LetterDetectedDelay) >  2000 ) and (lock_status == true) ){
    previousTimeToF = millis();
    measurement();
    if (distance < 60){
     LetterDetectedDelay = millis();
     LetterCounter = LetterCounter + 1 ; 
    }
  }
  
  if ((millis() - previousTimeScreen) > 500){
    previousTimeScreen = millis();
    screen_display(); 
  }

  if (request_BLE == "Ouverture"){
    Serial.println("Condition Ouverture");
    ros_publisher(0);
    lock_status = false;
  }
  if (request_BLE == "Fermeture"){
     Serial.println("Condition Fermeture");
     ros_publisher(1.6);
     lock_status = true;
  }
  if (request_BLE == "Cheese"){
     Serial.println("Condition Cheese");
  }
  if (request_BLE == "Letter"){
     Serial.println("Condition Letter");
  }

}

void setup_Serial(){
  //Setup BaudRate
  Serial.begin(57600);
}

void setup_ros(){
  nh.initNode();
  nh.advertise(motor_cmd);  
}

void setup_ToF(){
  // wait until serial port opens for native USB devices
  while (! Serial) {
    delay(1);
  }
  if (!lox.begin()) {
    Serial.println(F("Failed to boot VL53L0X"));
    while(1);
  }
}

void setup_Screen(){
  //Set up the display
  tft.init();
  tft.setRotation(3);
  tft.fillScreen(TFT_BLACK);
  tft.setTextSize(1);
  tft.setTextColor(TFT_WHITE);
  tft.setCursor(0, 0);
}

void setup_BLE() {
  // Create the BLE Device
  BLEDevice::init("ESP32_kyks");

  // Create the BLE Server
  pServer = BLEDevice::createServer();
  pServer->setCallbacks(new MyServerCallbacks());

  // Create the BLE Service
  BLEService *pService = pServer->createService(SERVICE_UUID);

  // Create a BLE Characteristic
  pCharacteristic = pService->createCharacteristic(
                      CHARACTERISTIC_UUID,
                      BLECharacteristic::PROPERTY_READ   |
                      BLECharacteristic::PROPERTY_WRITE  //|
                      //BLECharacteristic::PROPERTY_NOTIFY |
                      //BLECharacteristic::PROPERTY_INDICATE
                    );

  // Create a BLE Descriptor
  pCharacteristic->addDescriptor(new BLE2902());

  // Create reference to callback for write
  pCharacteristic->setCallbacks(new MyCallbacks());
  pCharacteristic->setValue("None");

  // Start the service
  pService->start();

  // Start advertising
  BLEAdvertising *pAdvertising = BLEDevice::getAdvertising();
  //BLEAdvertising *pAdvertising = pServer->getAdvertising();
  pAdvertising->addServiceUUID(SERVICE_UUID);
  pAdvertising->setScanResponse(false);
  pAdvertising->setMinPreferred(0x0);  // set value to 0x00 to not advertise this parameter
  BLEDevice::startAdvertising();
  //pAdvertising->start();
  Serial.println("Waiting a client connection to notify...");
  delay(50);
}

void ros_publisher(float data)
{
  motor_msg.data = data;
  motor_cmd.publish( &motor_msg );
  nh.spinOnce();
}


void screen_display(){
  //Display a simple splash screen
  tft.fillScreen(TFT_BLACK);
  tft.setTextSize(2);
  tft.setTextColor(TFT_WHITE);
  tft.setCursor(0, 0);
  tft.println(F("SmartBox"));
  tft.setTextColor(TFT_RED);
  tft.setCursor(0, 30);
  tft.println(F("Status : "));
  tft.setCursor(100, 30);
  if (lock_status == false){
    tft.println(F("Open"));
  }
  else{
    tft.println(F("Close"));
  }
  tft.setCursor(0, 60);
  tft.println(F("BLE : "));
  tft.setCursor(80, 60);
  if (deviceConnected == true){
    tft.println(F("Connected"));
  }
  else{
    tft.println(F("Disconnected"));
  }
  tft.setCursor(0, 90);
  tft.println(F("Letter Counter : "));
  tft.setCursor(200, 90);
  tft.println( String(LetterCounter) );  
  
}

void measurement() {
  VL53L0X_RangingMeasurementData_t measure;
    
  //Serial.print("Reading a measurement... ");
  lox.rangingTest(&measure, false);                 // pass in 'true' to get debug data printout!

  if (measure.RangeStatus != 4) {                   // phase failures have incorrect data
    Serial.print("Distance (mm): "); 
    Serial.println(measure.RangeMilliMeter);
  }
  else {
    Serial.println(" out of range ");
  }
  distance = measure.RangeMilliMeter ;
}
