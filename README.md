# Projet_Proto


## Composants & Software
- ESP32 TTGO-Display
- Raspberry Pi 4 (Ubuntu 18.04 & ROS melodic)
- Capteur ToF
- Servomoteur Dynamixel
- Boite carton, pièces et support

## Mise en oeuvre

Sur une Raspberry Pi 4 correctement configurée avec Ubuntu 18.04 et une distribution ROS melodic installée :
- Installer dans un workspace ROS le package dynamixel motors (code_servo/src).
- Compiler le workspace et sourcer.
- Lancer le fichier servo_activation.launch du package dynamixel_tutorials.

Sur une ESP32, téléverser le fichier main.ino.
Brancher le capteur ToF selon la datasheet.


## Fonctionnalités
- Ouverture/Fermeture à distance via l'applciation
- Statut de la box, de la connexion, et du nombre de lettre




