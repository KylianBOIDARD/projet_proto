#!/usr/bin/env python

from __future__ import print_function

from proto.srv import image
import rospy
import cv2
import rospkg
from cv_bridge import CvBridge



def handle_image(req):
    bridge = CvBridge()
    camera = cv2.VideoCapture(0)
    ret, frame = camera.read()
    
    
    image_message = bridge.cv2_to_imgmsg(frame, encoding="bgr8")
    return image_message

def image_server():
    rospy.init_node('image_server')
    s = rospy.Service('image', image, handle_image)
    print("Image zeubi")
    rospy.spin()

if __name__ == "__main__":
    image_server()
